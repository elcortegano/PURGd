##   PURGd INPUT README

The software PURGd is distributed with 24 example pedigree files. You can start running PURGd using these files as input pedigree files.

### Simulated data

Files named from `msd1.csv` to `msd10.csv`, and from `d1.csv` to `d10.csv`, correspond to subset of those analyzed in the publication introducing this software (García-Dorado *et al*. 2016). These were originated from simulated lines of reduced population size that were derived from a larger base population. Mutations were assumed to be recessive (h=0) with homozygous effect s=0.3, and arise at a rate of 0.1 per gamete and generation. 

In the first set of pedigree files (msd), natural selection operated both in the base population and in the small lines, so that purging is expected to occur. In the second one (d), natural selection was relaxed in the small lines, so that no purging is expected.

These files follow the convention in PURGd of reserving the first column to the individual identity (id), the second and third columns to dams (dam) and sires (sire), and the fourth one to biological fitness. A fifth column is included in these files indicating the generation number.

### Ungulate data

Real data from 4 ungulate endangered species is also provided, corresponding to the species *Ammotragus lervia* (`arrui.csv`), *Gazella cuvieri* (`atlas.csv`), *G. dorcas* (`dorcas.csv`) and *Nanger dama* (`dama.csv`). The studbook of these species, containing the complete pedigrees, can be found at the [EEZA website](http://www.eeza.csic.es/es/programadecria.aspx). Files distributed here have been already preprocessed for their inbreeding-purging analyses, and are a valuable data both for research, given their high pedigree completeness, long track records (starting in the 1960s-1970s), and low effective population sizes (Ne). Details on the processing of these files and other population parameters (eg. Ne, and number of generations) can be found in López-Cortegano et al. (unpublished).

These files follow again the convention in PURGd of reserving the first four columns to individual, mother and father identity (id, dam, an sire columns respectively), and a measure of fitness. In the files provided, the fourth column refers to fitness measured as 15-days survival (survival15), but female productivity is also included in a fifth column (prod). Additional columns contain, in order, the individual sex (sex), year of birth (yob), period of management (yom), a boolean value indicating whether the individual belongs to the target population (target), and the original individual identity as reported in the original studbooks. Additional details on these parameters can be found in the above mentioned reference.

### Cross-platforms use

The files provided will work the same in GNU/Linux and Windows environments.

However, we warn that these files have been written in a UNIX machine, so they are LF ended and may be hard to visualize in Windows (CR+LF ended). If opened with Excel in Windows, they can easily be converted for a friendly view (select the first column in the file, go to DATA, and choose "text in columns" > "delimited" > "comma").

### References

- García-Dorado A, Wang J, López-Cortegano E. 2016. Predictive model and software for inbreeding-purging analysis of pedigreed populations. G3 6: 3593-3601.

- López-Cortegano E, Moreno E, García-Dorado A. unpublished.
