# PURGd 2.3.2

PURGd is a software developed to detect purging and to estimate inbreeding-purging genetic (IP) parameters in pedigreed populations. The models and methods used in this software are described in García-Dorado *et al*. (2016).

The main objective of this program is to estimate the effective purging coefficient (*de*, hereafter referred to as *d* for simplicity) described by García-Dorado (2012), which is an overall genomic measure of the component of the deleterious effects that is only expressed in homozygosis and is therefore responsible for purging under inbreeding. Furthermore, the program estimates the regression coefficients on the purged inbreeding coefficient (*g*), here denoted *b(g)*, and on additional regressor variables, such as environmental factors or maternal inbreeding. This software also includes options to estimate parameters for purging models based on ancestral inbreeding, developed by Ballou (1997) and Boakes & Wang (2005).

Two alternative approaches were implemented in v2.0 and earlier releases, a linear regression method (LR) and a Numerical non-linear regression method (NNLR). Since the later is more flexible and powerful than the former, more recent releases only work with the NNLR method (García-Dorado *et al*. 2016; López-Cortegano *et al*. 2018). Under this method, the non-linear model for untransformed fitness is numerically explored by searching for the joint numerical LS estimates of *d* and of the non-linear regression coefficients. In this method, *b(g)* estimates the inbreeding load under the IP model (García-Dorado et al. 2016). PURGd also computes Wright's standard inbreeding coefficient *F*, Ballou's ancestral inbreeding coefficient *Fa* (Ballou 1997) and García-Dorado's purged inbreeding coefficient *g* (García-Dorado 2012), as well as the effect of other genetic and environmental factors of interest introduced in the model.

## Warning

**This software has been discontinued**, and its development has moved to the [purgeR](https://gitlab.com/elcortegano/purgeR) ([https://gitlab.com/elcortegano/purgeR](https://gitlab.com/elcortegano/purgeR)) R package (available at [CRAN](https://cran.r-project.org/web/packages/purgeR/index.html)), which include bug fixes, new features, as well as performance and usability improvements.

## Program folders

This software is distributed in a package that includes several folders. This is a quick overview on their content. More detailed information about each folder will be found in the following sections.

- `input`: example pedigree files.
- `man`: user's guide files.
- `src`: source code.
- `cpptools`: C++ source files for helper programmes (read cpptools/README.md)
- `rtools`: useful R scripts (read rtools/README.md).

Additionally, the package contains this user's guide in markdown (.md) format, a simpler README file, a Changelog, Makefile, Dockerfile, and a copy of the GNU GPL 3 License.

## Installation

The present software has been written in C++ language and is distributed as open-source, so it can be compilled in any platform. A docker image is also provided so that the program can be used without compilation on any operative system.

#### GNU/Linux:

PURGd can be easily compiled from source using the make utility.

```
cd PURGd
make
sudo make install
```

#### Docker:

Alternatively, you can also use [Docker](https://www.docker.com/) to download and safely run a PURGd docker image independently of the OS you use:

```
docker pull elcortegano/purgd:latest
```

Note that Windows users may require an updated system to use docker *i.e.* Windows 10 in a 64bit machine with virtualization enabled (this can be done in the BIOS/UEFI settings).

#### Windows:

PURGd can be compiled in Windows environments, using the same utilities as in GNU/Linux (*e.g.* the g++ compiler or the make tool). One choice is to compile the program in a [MSYS2/MinGW](http://mingw.org/wiki/msys) environment.

Note that under this environment, all required tools must be installed. After installing MSYS2/MinGW, open a MSYS terminal, and update the system: `pacman -Syu`, followed by `pacman -Su`. Then search for the required tools using the command `pacman -Ss <tool>`. For example, `curl` and `git` are required to be installed to download the repository. These can be installed with `pacman -S curl git`. Other tools may require information of the system architecture, as make. Search for `pacman -Ss make` and install the package that corresponds to your system architecture (*e.g.* `pacman -S mingw-w64-i686-cmake` if its a 64bit i686 system).

After these steps, compiling the software should be as easy as changing to its directory (`cd PURGd`) and running make (`make`). A binary (with the .exe extension) will be available in the msys32/home/ tree directory, usually under the C: disk (depending on how MSYS2/MinGW was installed). Windows binary files should be run in the Command Prompt (cmd) and not the PowerShell.

## Input

This program works pedigree files that can be placed in the existing input folder for convenience, although this is not mandatory. Example input files are distributed with the package, and can be found in the input folder.

### Pedigree files

Pedigree files are the fundamental input required by PURGd. These files must be in comma-separated values format (.csv) and have at least four columns, with the following precise order: identity of the individual (ID), mother ID, father ID, and the evaluation of fitness or of a fitness component trait. Fitness must be recorded using natural untransformed fitness numeric values (*i.e.*, non-logarithmic), but IDs can be numerical or strings of characters (excluding comma). The file must contain a header with the names of these variables in the first line.

Individuals should be ordered in the file from older to younger. Individuals whose parents are not present in the first column (*i.e.*, in the individual identity column) are assumed to be unrelated. This is so even if several of these parents have been coded with the same identity. Therefore, the pedigree file should include a record for each individual (with its identity in the first column), including unrelated founders. We advise coding the identity of the parents of unrelated founders or of individuals from the non-inbred base population as 0.

Missing values for fitness or for additional factors must be coded as NA.

![Pedigree file view as plain text (left) and as spreadsheet (right)](images/pedigree.png)

Figure above shows on the left a pedigree file named Qilin.csv with data for a fitness component trait (longevity) using a text editor, with no blanks, and comma (,) separated values; On the right: The same file is shown using a spreadsheet program, such as LibreOffice Calc. Extra columns can be added containing additional causal factors to be fitted in the model. These factors can be numeric (quantitative) or categorical (qualitative) variables. Similarly as for fitness, a value of NA will indicate missing observations. Note that including additional factors that are correlated to inbreeding may distort the estimates of inbreeding purging parameters.

## Start using PURGd

PURGd is a command-line program, and, as such, it should be launched from the terminal (GNU/Linux) or the cmd prompt (Windows). It must be run using the following general syntax:

```
./PURGd [ --options ] file
```

Here *options* refers to any setting parameter that, although not strictly required to run the program, will take preference over the default value if its specified. Therefore, the only mandatory argument to run the program is *file*, which is the name of the input file, preceded by its absolute or relative path.

Alternatively, by entering

```
./PURGd --help
```

the program will print a short manual to use it as a quick reference guide.

A simple example on how to run PURGd is given below:

```
./PURGd input/msd1.csv
```

In this example, the program will use the NNLR method assuming the IP model, which are the default options, to analyse the pedigree file msd1.csv. In this case, the relative path input/ is typed because our working directory is the root folder of PURGd and msd1.csv is located in the input folder.

To use Ballou's model instead of the default IP model, the example above has to be modified specifying the `--ffa` argument: by slightly modifying the example above:

```
./PURGd --ffa input/msd1.csv
```

Now PURGd will still use the NNLR method during the analysis of msd1.csv but using Ballou's model.

Other examples of use are:

```
./PURGd --verbose --limit=20 --delta=3 input/msd2.csv
./PURGd --seed=1234 --verbose --genedrop input/msd3.csv
```

Check the following sections to learn more about all the available settings and how to modify them to customise the analysis.

The corresponding output files will be saved in the working directory by default.

If you are going to use the docker image, remember that once downloaded it can be renamed and executed as:

```
docker tag elcortegano/purgd:latest purgd
docker run --rm -v $PWD:/tmp purgd [options] file
```

Where $PWD refers to your working directory. Remember to change the version flag (*e.g.* v2.0) if needed.

### Program settings

PURGd ships with a complete set of predefined **default values** for the settings parameters. Some of these values can be modified to change the way that input data is analyzed. This section describes all the available settings that can be modified by the user, as well as their corresponding default values.

Command line terminal options are preceded by a double hyphen (`--`). Any option entered directly from the command line terminal will always have preference over default values. Older releases of PURGd had a settings file as secondary way to introduce options, but was deprecated in version 2.3.2.

Available options are described below. Empty default values indicate that the parameter is disabled, while a crossmark (x) indicate that is enabled. Some command line options require an argument that is passed with the equality sign (=). It can be a file, path name or other string (NAME), an integer (INT), a real number (NUM), or a list of comma-separated variables (*eg.* INT,INT).

### General options

- Help: Prints a summary of command line terminal options.

- Seed: It is the seed used to generate pseudorandom numbers during the analysis. It can be convenient to set a fixed seed value in order to replicate the results obtained.

- Verbose mode: This mode prints a short summary in the terminal for each pedigree that is being analyzed. It is not used by default.

- Accuracy of the estimates: This parameter can also be modified if the user needs a better accuracy for inbreeding purging estimates. By default, *0.01* is used.

  Parameter     | Default  |   Command option   
----------------|----------|--------------------
  Help          |          | --help             
  Seed          |  time    | --seed=INT         
  Verbose mode  |          | --verbose          
  Accuracy      |  0.01    | --accuracy=NUM     

### Models

- Model: Defines the purging model to be used. It can be the inbreeding-purging model (García-Dorado 2012), Ballou's model (Ballou 1997), Boakes & Wang's model or their mixed model (Boakes & Wang 2005).

- No purging analyses: It is possible to estimate the inbreeding load (delta) without any purging analyses. To do so, the option `--only-delta` is used. Note delta here equals minus the slope on inbreeding, b(g), as introduced at the beggining of the guide. The estimated P value will be then referred to a null hypotesis with delta=0.

  Parameter              |  Default  |   Command option  
-------------------------|-----------|-------------------
  IP Model               |  x        |  --ip             
  Ballou's model         |           |  --ffa              
  Boakes & Wang's model  |           |  --fa              
  Mixed model            |           |  --faffa           
  No purging             |           |  --only-delta     

### Analysis options

- Genedrop: By default the program will estimate ancestral inbreeding coefficients using their pedigreed expected values, but simulated values can also be generated by using a gene dropping simulation process, which may be more convenient (Suwanlee et al. 2016). If passed the option without a value (`--genedrop`), defauly number of iterations will be *1000,000*.

- Fixed purging coefficient: A fixed value of the purging coefficient can be set (in the range *[0.0, 0.5]*), instead of estimating it.

- Initial average fitness (*W0*): A numeric value for this parameter can be set by the user if there is information available. By default (`noinbred`), it will be computed as the average fitness on non-inbred individuals with non-inbred ancestors (*F=Fa=0*). Initial fitness can also be estimated as a regressor variable (the intercept), however this last option can induce some overfitting in the model, and is only recommended when others options are not available or have high errors. This last option is set as `--w0`. Note that when estimating w0 as regressor, maximum explored value for w0 might be adjusted if it's higher than the default value of 1.0 (see --max-w0 option below).

- Inbreeding load (*delta*): This parameter can also be set as numeric by the user. However, by default, the inbreeding load is estimated at the same time as any other parameter in the corresponding model. It may also be estimated for a subset of individuals with non-inbred ancestors (*Fa=0*) in the original pedigree (`--delta-nopurged`), and then used as a fixed parameter.

- Use of maternal effects: Maternal effects can be incorporated in the analysis as the (purged) inbreeding coefficient of the dams.

- Use of additional factors: Several other numeric and/or categorical factors can be incorporated into the genetic model.

  Parameter                  |  Default  |  Command option                 
-----------------------------|-----------|---------------------------------
  Genedrop                   |           |  --genedrop=INT                 
  Fixed purging coefficient  |           |  --d=NUM                        
  Initial fitness            |  noinbred |  --w0=NUM / --w0                
  Inbreeding load            |           |  --delta=NUM / --delta-nopurged  
  Maternal effects           |           |  --maternal                     
  Numeric factor             |           |  --factor.cols=INT,...          
  Numeric factor names       |           |  --factor.names=NAME,...        
  Categorical factors        |           |  --cfactor.cols=INT,...         
  Categorical factor names   |           |  --cfactor.names=NAME,...       

### Output options

- Save a database: Two options allow to save a database (see Output section below) containing fitness and the inbreeding coefficients, either for the analyzed individuals or all in the pedigree file (full database).

  Parameter      |  Default  |  Command option    
-----------------|-----------|--------------------
  Save database  |           |  --save-db         
  Save full db   |           |  --save-db-all     

### NNLR options

- Number of bees: Define the number of solutions to explore by iteration.

- Limit for convergence: Required number of consecutive iterations without improving results.

- Number of runs: This settings allows the user to run the ABC algorithm of the NNLR method more than once for each pedigree. Results are averaged. It is convenient to run each analysis several times to check the stability of the results.

- Maximum value of initial fitness (*W0*): Values of the initial fitness parameter can be explored from *0* to a maximum value defined by the user when estimating it as a regressor variable (eg. using `--w0`). In that case, the maximum value us 1 by default. Large values for initial fitness can slow the program, making convenient to scale fitness by a constant to reduce maximum values.

- Maximum value of the inbreeding depression rate (*delta*): Values of the inbreeding depression rate can be explored from *0* to a maximum value defined by the user. Default is 10.

- Range of the slope for other regression terms: If additional factors are used, the value of their regression terms can be explored from a inimum to a maximum value defined by the user. By default, this range is *[-10,10]*.

  Parameter                |  Default     |  Command option            
---------------------------|--------------|----------------------------
  Number of bees           |  250         |  --nbees=INT               
  Limit                    |  10          |  --limit=INT               
  Number of runs           |  1           |  --nruns=INT               
  Maximum W0 value         |  1.0         |  --max-w0=NUM              
  Maximum $\delta$ value   |  10.0        |  --max-delta=NUM           
  Aditional factors range  |  -10.0,10.0  |  --factor.range=NUM,NUM    

## Output files

A short summary can also be displayed on the screen when the program has finished (using verbose mode). More complete results will be stored in output files.

![Example of output summary](images/output_example.png)

- Note: If the analyses is run with `only-delta` or `delta-nopurged`, the summary will print that value and not the purging coefficient. This value equals the slope on the inbreeding coefficient with the sign changed. If Fa-based models are used, the value of the slope with the Fa term will be printed.

Output files are stored automatically once the program finishes the analysis. These files are also in *csv* format, so that they can be easily converted for a friendly view if opened with a spreadsheet program (For example, with LibreOffice Calc).

- Note: Each output file is named by default after its corresponding input file and the chosen model and method for analysis. This means that any new analysis with the same input model and method will overwrite the existing file.

PURGd uses three kind of output files, which are described below.

### Output files for *d* and the regression coefficients estimates

Analysis performed using the NNLR method will save a file with the *_model.csv* extension flag.

Two output sets are shown in these files, one for the pertinent analysis performed considering purging, with the estimates of *d* or the regression coefficients corresponding to regressor variables defined in terms of *Fa*, and another one for an analogous analysis assuming no purging (conditional to a *0* value for *d* or for those coefficients). Comparing these two analyses shows how far fitting improves by considering purging. Note that if options to estimate only the inbreeding load are used (eg. `--delta-only`, `--delta-nopurged`), only results assuming no purging will be returned, and the statistic test will be run against the null hypothesis of delta=0.

The standard output consists of the following columns:

- **d coefficient**: The estimated (or assumed) effective purging coefficient.
- **RSS**: The residual sum of squares.
- **AICc**: The corrected Akaike Information Criterion.
- **RL**: The relative likelihood compared to other models (a value of 1 indicates the best model).
- **Chi2**: The Pearson's Chi-squared score of the purging model to be tested against the non-purging model.
- **p-value (Chi2)**: The p-value associated to the Chi-squared test.
- **W0**: The initial non-inbred mean for log-fitness or for untransformed fitness.
- **b[*factor*]**: The regression coefficient for each factor included in the analysis (including that for the purged inbreeding coefficient term *g*, regressor variables defined in terms of *Fa* or other additional factors). Note that b(g) here refers to minus delta (the inbreeding depression rate).
- **SE[*parameter*]**: The standard error for the estimated parameters.

- Warning: Analyses with fixed value of *d* (using `--d` option), and estimating w0 as regressor (using `--w0` option) result in unaccurate estimates of AICc and P values. We recommend using the RSS values returned for statistical testing while the issue is handled.

### Databases

If specified in the program settings, databases with the *_data.csv* extension in their file name will be saved. The database file may include the following columns, depending on the settings:

- **Identity**: The identity of the individual.
- **W**: Fitness, as used in the analysis, so the scale may be different than the one in the input file.
- **F**: Standard inbreeding coefficient
- **g(d)**: Purged inbreeding coefficient, computed with the estimate obtained for *d*.
- **Fa**: Ancestral inbreeding coefficient.
- **Fa(genedrop)**: Ancestral inbreeding coefficient, estimated by gene dropping (if enabled).
- **gdam(d)**: Maternal purged inbreeding coefficient, computed with the estimate obtained for *d* (if enabled).
- **Effects of additional factors** in the input (if used).

Note that individuals excluded from the inbreeding-purging analysis (*e.g.*: individuals with unknown fitness) are excluded by default with the option `--save-db` and will not appear in this output file. The option `--save-db-all` can be used to record their inbreeding values as well.

## About

This Software was developed by Eugenio López-Cortegano.

We thank other contributors to the software: Isauro López-Cortegano, Diego Bersabé, Jinliang Wang, Aurora García-Dorado.

PURGd is a free software oriented to research, non-commercial use, and it is distributed under the terms described in the LICENSE file.

If you use PURGd in your research, cite:

- GARCÍA-DORADO, A., WANG, J., LÓPEZ-CORTEGANO, E. (2016) *Predictive model and software for inbreeding-purging analysis of pedigreed populations*. G3: Genes, Genomes, Genetics 6 (11): 3593-3601.

Users are encouraged to request additional features on the software and to report bugs. In that case, please contact Eugenio López-Cortegano (<e.lopez-cortegano@ed.ac.uk>).

## Bibliographic references

BALLOU, J.D. (1997). *Ancestral inbreeding only minimally affects inbreeding depression in mammalian populations*. Journal of Heredity 88 (3): 169-178.

BOAKES, E., WANG, J. (2005). *A simulation study on detecting purging of inbreeding depression in captive populations*. Genet. Research 86: 139-148.

BURNHAM, K.P., ANDERSON, D. R. (2002). *Information and likelihood theory: A basis for model selection and inference in Model selection and multimodel inference*. (pp. 49-96). Springer.

GARCÍA-DORADO, A (2012). *Understanding and predicting the fitness decline of shrunk populations: inbreeding, purging, mutation, and standard selection*. Genetics 190: 1461-1476.

GARCÍA-DORADO A., WANG, J., LÓPEZ-CORTEGANO, E. (2016). *Predictive model and software for inbreeding-purging analysis of pedigreed populations*. G3: Genes, Genomes, Genetics 6 (11): 3593-3601.

LÓPEZ-CORTEGANO E., BERSABÉ, D., WANG, J., GARCÍA-DORADO, A. (2018). *Detection of genetic purging and predictive value of purging parameters estimated in pedigreeed populations*. Heredity 121 (1): 38-51.

SUWANLEE, S., BAUMUNG, S., SÖLKNER, J., CURIK, I. (2016). *Evaluation of ancestral inbreeding coefficients: Ballou's formula versus gene dropping*. Conservation genetics 8: 489-495.

WEISBERG, S. (2005). *Multiple regression in Applied linear regression* (pp.47-68). Wiley-Interscience.
