#ifndef TEMPLATES_H
#define TEMPLATES_H

// =====   HEADER FILE INCLUDES   =====
#include <algorithm>
#include <map>
#include "alias.h"
#include "error.h"


// =====   INITIALIZED VARIABLES  =====

extern time_t SEED;

extern std::mt19937 random_generator; // Mersenne Twister pseudorandom number generator engine


// =====   FUNCTION TEMPLATES  =====

/* =======================================================================================
 * Function readmap ()
 * ---------------------------------------------------------------------------------------
 * Find values in a map given a key as usual, but allows to return values by default
 ======================================================================================= */
template <typename K, typename V>
V readmap (const std::map<K,V>& m, const K& key, const V& def) {
	typename std::map<K,V>::const_iterator it = m.find(key);
	if (it==m.end()) return def;
	else return it->second;
}

/* =======================================================================================
 * Function max ()
 * ---------------------------------------------------------------------------------------
 * Returns the maximum row size in a matrix.
 ======================================================================================= */
template<typename T>
size_t max (const std::vector<std::vector<T>>& matrix) {
	size_t max_ (0), nrows(matrix.size());
	for (size_t i(0); i<nrows; ++i) if (matrix[i].size() > max_) max_ = matrix[i].size();
	return max_;
}

/* =======================================================================================
 * Function check_min_col ()
 * ---------------------------------------------------------------------------------------
 * True if a matrix have at least min columns
 ======================================================================================= */
template<typename T>
bool check_min_col (const std::vector<std::vector<T>>& m, std::size_t min) {
	for (const auto& r: m) if (r.size()<min) return false;
	return true;
}

/* =======================================================================================
 * Function check_var_row ()
 * ---------------------------------------------------------------------------------------
 * True if a matrix have rows variating in the number of columns
 ======================================================================================= */
template<typename T>
bool check_var_row (const std::vector<std::vector<T>>& m) {
	std::size_t first (m[0].size());
	for (std::size_t i(1); i<m.size(); ++i) if (m[i].size()!=first) return true;
	return false;
}

/* =======================================================================================
 * Function column_to_vector ()
 * ---------------------------------------------------------------------------------------
 * Gets one column from a matrix, and returns a vector containing it.
 ======================================================================================= */
 template<typename T>
 std::vector<T> column_to_vector (const std::vector<std::vector<T>>& M, size_t col) {
	 std::vector<T> vector;
	 for(size_t i(0),ncols(M.size()); i<ncols; ++i) {
		 vector.push_back(M[i][col]);
	 }
	 return vector;
}

/* =======================================================================================
 * Function range_to_vector ()
 * ---------------------------------------------------------------------------------------
 * Created a vector from a set of values: minimum, maximum and step.
 ======================================================================================= */
template<typename T>
std::vector<T> range_to_vector (T min, T interval, T max) {
	std::vector<T> new_vector (1,min);
	if ((min==max) || !interval) return new_vector;

	T value (min);
	size_t N ((max-min)/interval);
	for (size_t i(0); i<N; ++i) {
		value += interval;
		new_vector.push_back(value);
	}
	return new_vector;
}

/* =======================================================================================
 * Function vector_to_matrix ()
 * ---------------------------------------------------------------------------------------
 * Converts a vector in a single column matrix.
 ======================================================================================= */
template<typename T>
std::vector<std::vector<T>> vector_to_matrix (std::vector<T> column) {
	std::vector<std::vector<T>> matrix;
	for (size_t i(0); i<column.size(); ++i) matrix.push_back({column[i]});
	return matrix;
}

/* ------------------------------------------------------------------------------------- */
template<typename T>
std::vector<std::vector<T>> vector_to_matrix (std::size_t N, T val) {
	std::vector<std::vector<T>> matrix;
	for (size_t i(0); i<N; ++i) {
		std::vector<T> row (1,val);
		matrix.push_back(row);
	}
	return matrix;
}

/* =======================================================================================
 * Function rm_row ()
 * ---------------------------------------------------------------------------------------
 * Removes rows in a matrix.
 ======================================================================================= */
template<typename T>
std::vector<std::vector<T>> rm_rows (const std::vector<std::vector<T>>& matrix, const std::vector<bool>& eval) {

	std::vector<std::vector<T>> new_matrix;
	for (size_t i(0), nrow (matrix.size()); i<nrow; ++i) {
		std::vector<T> row;
		for (size_t j(0), ncol (matrix[i].size()); j<ncol; ++j) {
			if (eval[i]) row.push_back(matrix[i][j]);
		}
		if (eval[i]) new_matrix.push_back(row);
	}
	return new_matrix;
}

/* =======================================================================================
 * Function rm_col ()
 * ---------------------------------------------------------------------------------------
 * Removes columns in a matrix.
 ======================================================================================= */
template<typename T>
std::vector<std::vector<T>> rm_col (const std::vector<std::vector<T>>& matrix, std::size_t index) {

	std::vector<std::vector<T>> new_matrix;
	for (size_t i(0), nrow (matrix.size()); i<nrow; ++i) {
		std::vector<T> row;
		for (size_t j(0), ncol (matrix[i].size()); j<ncol; ++j) {
			if (j!=index) row.push_back(matrix[i][j]);
		}
		new_matrix.push_back(row);
	}
	return new_matrix;
}

/* =======================================================================================
 * Function add_column ()
 * ---------------------------------------------------------------------------------------
 * Ass a new column to a matrix. The position of the column can be defined.
 ======================================================================================= */
template<typename T>
void add_column (std::vector<std::vector<T>>& matrix, const std::vector<T>& vector, size_t col) {
	for (size_t i(0); i<matrix.size(); ++i)	matrix[i].insert(matrix[i].begin()+col,vector[i]);
}

/* ------------------------------------------------------------------------------------- */
template<typename T>
void add_column (std::vector<std::vector<T>>& matrix, T value, size_t col) {
	for (size_t i(0); i<matrix.size(); ++i)	matrix[i].insert(matrix[i].begin()+col,value);
}

/* =======================================================================================
 * Function merge ()
 * ---------------------------------------------------------------------------------------
 * Appends a vector to the end of another
 ======================================================================================= */
template<typename T>
std::vector<T> merge (std::vector<T> v1, const std::vector<T>& v2) {
	std::vector<T> r (v1);
	for (const auto& i: v2) v1.push_back(i);
	return v1;
}

/* =======================================================================================
 * Function repetition ()
 * ---------------------------------------------------------------------------------------
 * Look for repeated elements in a vector
 ======================================================================================= */
template<typename T>
bool repetition (std::vector<T> vector1) {

	bool repetition (false);
	size_t N(vector1.size());
	for (size_t i(0); i<N; ++i) {
		for (size_t j(i+1); j<N; ++j) {
			if (vector1[i] == vector1[j]) {
				repetition = true;
				break            ;
			}
		}
	}
	return repetition;
}

/* =======================================================================================
 * Function cbind ()
 * ---------------------------------------------------------------------------------------
 * Merge two matrix by column (matrix2 is added to the right side of matrix1).
 ======================================================================================= */
template<typename T>
void cbind (std::vector<std::vector<T>>& matrix1, const std::vector<std::vector<T>>& matrix2) {

	if (matrix1.size() != matrix2.size()) {
		std::cerr << "ERROR: matrix1 does not have the same number of rows than matrix2 [cbind()]\n";
	}

	for (size_t i(0); i<matrix2.size(); ++i) {
		for (size_t j(0); j<matrix2[i].size(); ++j) {
			matrix1[i].push_back(matrix2[i][j]);
		}
	}
}

/* =======================================================================================
 * Function variation ()
 * ---------------------------------------------------------------------------------------
 * If there is variation between elements in a vector, it returns true.
 ======================================================================================= */
template<typename T>
bool variation (std::vector<T> vector) {
	const T begin (vector[0]);
	for (size_t i(1),N(vector.size()); i<N; ++i) if (vector[i]!=begin) return true;
	return false;
}

/* =======================================================================================
 * Function is_in ()
 * ---------------------------------------------------------------------------------------
 * Check the presence of at least one element in a vector or string.
 ======================================================================================= */
template<typename T>
inline bool is_in (T i, const std::vector<T>& v) {
	return (std::find(v.begin(), v.end(), i) != v.end());
}

/* ------------------------------------------------------------------------------------- */
inline bool is_in (char c, const std::string& s) {
	return (s.find(c) != std::string::npos);
}

/* =======================================================================================
 * Function correct_vector ()
 * ---------------------------------------------------------------------------------------
 * Corrects a vector in terms of a boolean vector that indicates proper values.
 ======================================================================================= */
template<typename T>
std::vector<T> correct_vector (std::vector<T> old_vector, bVector eval) {
	std::vector<T> new_vector;
	for (size_t i(0),N(old_vector.size()); i<N; ++i) if (eval[i]) new_vector.push_back(old_vector[i]);
	return new_vector;
}

/* =======================================================================================
 * Function sample ()
 * ---------------------------------------------------------------------------------------
 * Takes a random sample from a population of data in a vector.
 ======================================================================================= */
template<typename T>
T sample (std::vector<T> population) {
	std::uniform_int_distribution<> dist(0, population.size() - 1); // for random sampling of integers on the half-closed interval [0, population.size())
	size_t X ((dist(random_generator)));
	return population[X];
}

/* =======================================================================================
 * Function find ()
 * ---------------------------------------------------------------------------------------
 * Returns coordenate number of a value in a sample vector.
 ======================================================================================= */
template<typename T>
int find (T value, std::vector<T> sample) {
	for (size_t i(0); i<sample.size(); ++i) if (value == sample[i]) return i;
	return -1;
}

/* =======================================================================================
 * Function abs ()
 * ---------------------------------------------------------------------------------------
 * Returns the absolute value of a number.
 ======================================================================================= */
template<typename T>
T abs (T n) {
	if (n < 0) return -n;
	else return n;
}

/* =======================================================================================
 * Function pmatrix () & pvector ()
 * ---------------------------------------------------------------------------------------
 * This function prints a given matrix or vector. Its is used for testing.
 ======================================================================================= */
template<typename T>
void pmatrix (const std::vector<std::vector<T>>& m) {
	for (auto const& v: m) {
	for (auto const& i: v) {
		std::cout <<  i << " " ;
	}   std::cout << std::endl; }
}

/* ------------------------------------------------------------------------------------- */
template<typename T>
void pmatrix (const std::vector<std::vector<T>>& m, bVector eval) {
	for (size_t i(0); i<m.size(); ++i) {
		for (size_t j(0); j<m[i].size(); ++j) {
			if (eval[i]) std::cout << m[i][j] << " " ;
		}	if (eval[i]) std::cout << std::endl;
	}
}

/* ------------------------------------------------------------------------------------- */
template<typename T>
void pvector (const std::vector<T>& v) {
	for (auto const& i: v) {
		std::cout << i << " " ;
	}	std::cout << std::endl;
}

/* ------------------------------------------------------------------------------------- */
template<typename T>
void pvector (const std::vector<T>& v, bVector eval) {
	for (size_t i(0); i<v.size(); ++i) {
		if (eval[i]) std::cout << v[i] << " " ;
	}	std::cout << std::endl;
}


#endif
