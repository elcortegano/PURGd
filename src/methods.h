#ifndef METHODS_H
#define METHODS_H

// =====   HEADER FILE INCLUDES   =====

#include <iomanip>
#include "abc.h"
#include "save.h"


// ====   FUNCTION'S  PROTOTYPES   ====

void nnlr_method  ( name , Settings& );

#endif
