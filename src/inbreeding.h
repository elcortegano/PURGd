#ifndef INBREEDING_H
#define INBREEDING_H

// =====   HEADER FILE INCLUDES   =====

#include <set>
#include <unordered_map>
#include <utility>
#include <memory>
#include "alias.h"
#include "error.h"
#include "operators.h"
#include "templates.h"


// =====   INITIALIZED VARIABLES  =====

constexpr unsigned icol (0);
constexpr unsigned dcol (1);
constexpr unsigned scol (2);
constexpr unsigned wcol (3);
extern bool VERBOSE;

// ==== COANCESTRY MATRIX ====
struct pair_hash {
	template <class T1, class T2>
	std::size_t operator() (const std::pair<T1, T2> &pair) const {
		return std::hash<T1>()(pair.first) ^ std::hash<T2>()(pair.second);
	}
};

typedef std::unordered_map<coord, std::shared_ptr<coefficient>, pair_hash> coancestry_map;

class coancestry_matrix {

	public:
	coancestry_map coancestry;
	coefficient& operator[] (coord c) {
		if (c.first>c.second) c = std::make_pair (c.second, c.first);
		coancestry_map::iterator it = coancestry.find(c);
		if (it!=coancestry.end()) coancestry[c] = std::make_shared<double> (*it->second);
		else if (c.first==c.second && c.first) coancestry[c] = std::make_shared<double> (0.5);
		else coancestry[c] = std::make_shared<double> (0.0);
		return (*coancestry[c]);
	}
};

// ==== CLASS INBREEDING ====

class Inbreeding {

	protected:
	std::size_t N;
	std::size_t Npairs; // number of coancestries to compute
	pVector inbreeding; // inbreeding values
	Vector Fa; // Ballou's ancestral inbreeding
	Vector Ff;
	iVector t; // vector of generations
	std::map<std::size_t, std::size_t> t_map; //returns the first individual (value) of a generation (key)
	std::set<coord> required_pairs;

	public:
	Inbreeding () { N=0; Npairs=0; }
	void calc_Fa ( const iMatrix& );
	Vector get_F () { return (*inbreeding[0]); }
	Vector get_Fa () { return Fa; }
	Vector get_Ff () { return Ff; }

	void minimum_coancestry ( const iMatrix& );
	Vector compute_purging (const iMatrix& , const coefficient& );
	Vector m_inbreeding ( const iMatrix& , const Vector& );

	protected:
	void init ( const iMatrix& );
	void define_t ( const iMatrix& );
	void search_ancestors ( const iMatrix& , std::set<coord>& , const std::size_t& , const std::size_t& );

};

#endif
