/* =======================================================================================
 *                                                                                       *
 *       Filename:  input.cpp                                                            *
 *                                                                                       *
 *    Description:  This file contains functions for read, check and work with input     *
 * 					files.                                                               *
 *                                                                                       * 
 *                  -> input_Smatrix ()                                                  *
 *                  -> get_path ()                                                       *
 *                  -> get_filename ()                                                   *
 *                                                                                       *
 ======================================================================================= */

#include "input.h"


/* =======================================================================================
 * Function input_Smatrix ()
 * ---------------------------------------------------------------------------------------
 * This function reads text files and stores them in a matrix of type std::string. The
 * returned matrix can contain a header or not.
 ======================================================================================= */
sMatrix input_Smatrix (name filename, bool return_header) {

	name value;
	sMatrix array;
	int cont (0);

	name data (filename);
	std::ifstream file (data);

	if (file.is_open() ) {

		while ( getline (file, value) ) {

			if (value!=EMPTY) {
				sVector ROW;
				std::istringstream iss (value);
				name result;
				while (getline(iss, result, ',')) {
					if (result.size() && result[result.size()-1] == '\r') result = result.substr( 0, result.size() - 1 );
					if (!result.size()) throw_error(ERROR_EMPT);
					ROW.push_back(result.c_str());
				}
				if (cont || return_header) array.push_back(ROW);
			}
			++cont;
		}
		file.close();

	} else {
		pequal(pe,'='); std::cerr << ERROR_INPT << " " << filename << "\n";
		pequal(pe,'='); exit(-1);
	}

	return array;
}

/* =======================================================================================
 * Function get_path ()
 * ---------------------------------------------------------------------------------------
 * Assumes that PATH are a string and folders are '/' delimited. It uses split_vector()
 * to get the full string from the start to the last '/'.
 * =====================================================================================*/
name get_path (name string) {

	#ifdef _WIN32
		normalize_path(string);
	#endif

	std::string path (EMPTY);
	sVector full_path (split_vector(string,SEP));
	for (size_t i(0); i<full_path.size()-1; ++i) path = path + full_path[i] + SEP;
	return path;
}

/* =======================================================================================
 * Function get_filename ()
 * ---------------------------------------------------------------------------------------
 * Takes an string and takes the name in between the last '/' delimiter and the first '.'
 * extension sign.
 * =====================================================================================*/
name get_filename (name string) {

	#ifdef _WIN32
		normalize_path(string);
	#endif

	std::string filename (EMPTY);
	sVector path (split_vector(string,SEP));
	filename = path[path.size()-1];
	return (split_vector(filename,'.'))[0];
}

// =====   WINDOWS SPECIFIC   =====

/* =======================================================================================
 * Function normalize_path ()
 * ---------------------------------------------------------------------------------------
 * This function replaces all occurrences of the Windows separator \\ by /.
 ======================================================================================= */
#ifdef _WIN32
void normalize_path(name& path) {
	std::replace(path.begin(), path.end(), WIN_SEP, SEP);
}	
#endif