#ifndef ALIAS_H
#define ALIAS_H

#include <vector>
#include <string>
#include <random>
#include <algorithm>

// ============   ALIAS    ============

typedef double               coefficient;
typedef std::vector<coefficient>  Vector;
typedef std::vector<std::string> sVector;
typedef std::vector<size_t>      iVector;
typedef std::vector<bool>        bVector;
typedef std::vector<Vector>       Matrix;
typedef std::vector<sVector>     sMatrix;
typedef std::vector<iVector>     iMatrix;
typedef std::vector<bVector>     bMatrix;
typedef std::vector <Vector*>    pVector;
typedef std::vector <iVector*>  piVector;
typedef std::vector <Matrix*>    pMatrix;
typedef std::vector <iMatrix*>  piMatrix;
typedef std::string                 name;
typedef unsigned              individual;

typedef std::pair<std::size_t,std::size_t> coord;
typedef std::uniform_real_distribution<coefficient> Unif;

// =============   ENUM    ============

// Methods implemented in PURGd
enum Method {
	NNLR = 2,  // NNLR method
};

// Genetic models
enum Model {
	ID,  // Inbreeding depression model
	IP,  // Inbreeding - Purging (IP) model
	BA,  // Ballou's model
	BW,  // Boakes & Wang model
	MX   // Ballou - Boakes & Wang mixed model
};

// Code for correcting (or not) input IDs
enum Correct_ID {
	NO = 0,   // No correction needed (IDs are numbers from 1 to N)
	NB = 1,   // IDs are number and must be reordered.
	ST = 2,   // IDs include strings and must be recoded completely
};

// ===========   STRUCTS    ===========

// Range: limit values defined for a range //
struct Range {
	coefficient min;
	coefficient max;
	coefficient step;
};

struct Parameter {
	coefficient value = 0.0;
	coefficient ET = 0.0;
	coefficient stat  = 0.0;
};

struct RegStats {
	Vector v_RSS;
	coefficient RSS;
};

struct RegResults {
	coefficient d;
	Parameter w0;
	Parameter delta; // the inbreeding depression rate (min=0.0)
	std::vector<Parameter> reg_coefficients;
	Matrix factors;
	RegStats stats;
};
typedef RegResults Nectar;

inline std::vector<Parameter> create_parameters(const Vector& params) {
    std::vector<Parameter> ret(params.size());
    std::transform(params.begin(), params.end(), ret.begin(), [](auto value) { return Parameter{value, 0, 0};});
    return ret;
}

inline Vector get_parameter_values (const std::vector<Parameter>& params) {
	Vector ret;
	for (const auto& i: params) ret.push_back(i.value);
	return ret;
}

#endif
