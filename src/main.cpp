/* =======================================================================================
 *                                                                                       *
 *       Filename:  main.cpp                                                             *
 *                                                                                       *
 *    Description:  PURGd is a software to detect purging and to estimate inbreeding-    *
 *                  purging genetic parameters in pedigreed populations.                 *                                                           *
 *                                                                                       *
 *                  This file initializes the program reading ordinary pedigree files    *
 *                  and calling different methods (only NNLR by now).                    *
 *                                                                                       *
 *                  Please, refer to the user manual to know more about input and output *
 *                  files, and how the program works.                                    *
 *                                                                                       *
 *                                                                                       *
 *        Version:  2.3.2                                                                *
 *        Created:  01/04/2014                                                           *
 *        Updated:  13/11/2020                                                           *
 *       Compiler:  g++ (10.1.0)                                                          *
 *          Flags:  -Wall -c "%f" -std=c++17                                             *
 *                  -Wall -o "%e" "%f" -lgsl -lgslcblas -std=c++17                       *
 *                                                                                       *
 *         Author:  Eugenio López-Cortegano                                              *
 *          Email:  e.lopez-cortegano@ed.ac.uk                                           *
 *   Organization:  University of Edinburgh                                              *
 *                                                                                       *
 ======================================================================================= */

#include "main.h"


int main ( int argc , char *argv[] ) {

	/* READ SETTINGS FILE */
	Settings settings;
	settings.read_arguments (argc,argv);
	settings.check();

	/* LOCAL VARIABLES */
	name pedigree_file (argv[argc-1]);
	Method method (settings.get_method());
	FILE_NAME = get_filename (pedigree_file);
	name PATH (get_path(pedigree_file));

	/* ESTIMATION METHODS */
	random_generator.seed(settings.get_seed()); // seed the generator

	/* NON LINEAR NUMERICAL METHOD */
	if (method == NNLR) nnlr_method (pedigree_file, settings);
	else throw_error (ERROR_UNKM);

	/* SAVE LOG */ // Deprecated in v2.3.1
	//if (settings.get_save_log()) save_log (settings,FILE_NAME+MM_FLAG);

	/* THE END */
	pequal(pe,'='); std::cout << "Finish!" << std::endl; pequal(pe,'=');
	return 0;
}
