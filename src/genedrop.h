#ifndef GENEDROP_H
#define GENEDROP_H

// =====   HEADER FILE INCLUDES   =====
#include "alias.h"
#include "error.h"
#include "operators.h"

// =====   INITIALIZED VARIABLES  =====
constexpr unsigned da_col (3); // in the genotyped_pedigree matrix, column for the allele inherited from the dam
constexpr unsigned sa_col (4); // idem for the allele inherited from the sir
constexpr unsigned da_ibd (5); // in the genotyped_pedigree matrix, column for the allele inherited from the dam
constexpr unsigned sa_ibd (6); // idem for the allele inherited from the sir
extern bool VERBOSE;
extern std::mt19937 random_generator; // Mersenne Twister pseudorandom number generator engine

// =====   FUNCTION'S PROTOTYPES   =====
size_t sim_allele ( const iMatrix& , size_t , size_t , size_t& , Unif& );
void ibd ( iMatrix& );
Vector read_F (const iMatrix& );
Vector read_Fa (const iMatrix& );
Vector genedrop ( const iMatrix& , size_t );

#endif
