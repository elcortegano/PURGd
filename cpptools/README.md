##   PURGd CPPTOOLS README

### Overview

This folder contains accesory software for computing population genetics parameters.

Program files have a `.cpp` extension. Shell (`.sh`) scripts are given to compile these programs. They are always prefixed with "compile_", and followed by the name of the program.

For example, `pedigree_t.cpp` can be compiled by running:

```
bash compile_pedigree_t.cpp
```

This will require the g++ compiler to be installed in the system.

Note that applications below will return their output to the standard output (i.e. it will be printed on the terminal). Then, it may be necessary to redirect that output to a file for using it as an input.

### pedigree_rename

The accesory software <pedigree_rename> takes any pedigree file and returns an equivalent file with all individuals named and sorted from 1 to N. This program takes a pedigree file as input:

```
./pedigree_rename <pedigree_file.csv>
```

As mentioned above, the output can be redirected to a file, so it can be used as input in PURGd, eg:

./pedigree_rename file.csv > file_renamed.csv

Caution must be taken however if the input file contains founders with other name rather than '0', as the program then will return their IDs to the standard error.

### pedigree_cleaner

This program removes all individuals that will not be used in an inbreeding-purging analysis. That is, all individuals with NA values of fitness, that are not ancestors of any individual with valid values of fitness. This way, large pedigrees can be much shortened, improving the performance of long analyses.

### pedigree_completeness

Estimates the pedigree completeness level of a given pedigree file.

### pedigree_ancestors

Computes the number of founders and ancestors of a pedigree, as well as their effective numbers and the number of founder genome equivalents, following methods described by Boichard et al (1997) and Gutiérrez et al (2009) (see full citations in rtools/pedigree_t_Ne.R).

This program requires again a pedigree file as input, but this file must include a column named 'reference', indicating whether individuals belong to the reference population (1) or not (0). Estimates of ancestry will be referred to that reference population.

### pedigree_t

This software is designed to estimate the number of equivalent complete generations for a given individual in a pedigree file.

To run it, enter:

```
./pedigree_t <pedigree_file.csv> [<index>]
```

Where <index> refers to the name of the individual. This parameter is optional, if it is not entered, *t* will be computed for all individuals, and a pedigree file will be returned to the standard output, including a new column with the value of *t*.

Note that this software requires that individuals are named and sorted from 1 to N. Otherwise, it will break.
