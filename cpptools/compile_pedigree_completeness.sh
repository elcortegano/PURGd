#!/bin/sh

# Compilation instruction for pedigree_completeness
g++ -std=c++17 -o pedigree_completeness pedigree_completeness.cpp ../src/input.cpp ../src/utils.cpp
