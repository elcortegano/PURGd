#include "../src/alias.h"
#include "../src/error.h"
#include "../src/templates.h"

sMatrix input_Smatrix (std::string, bool);

constexpr std::size_t col_i = 0;
constexpr std::size_t col_dam = 1;
constexpr std::size_t col_sir = 2;

/* =======================================================================================
 * pedigree_rename: 
 * rename individuals with identity values from 1 to N
 ======================================================================================= */

int main (int argc, char *argv[]) {

	if (argc != 2) {
		std::cerr << "ERROR: Invalid number of arguments. Check the README file." << std::endl;
		exit(-1);
	}

	name filename = argv[1];
	sMatrix pedigree = input_Smatrix(filename, true);

	// Check if rename is needed
	bool rename (false);
	for (std::size_t i(1); i<pedigree.size(); ++i) {
		if (pedigree[i][col_i] != std::to_string(i)) {
			rename = true;
			break;
		}
	}

	if (rename) {
		// Set to zero all founder parents
		for (std::size_t i(1); i<pedigree.size(); ++i) {
			name obs_mom = pedigree[i][col_dam];
			name obs_dad = pedigree[i][col_sir];
			if (obs_mom != "0") {
				bool is_mom (false);
				for (std::size_t j(1); j<pedigree.size(); ++j) {
					if (pedigree[j][col_i]==obs_mom) {
						is_mom = true;
						break;
					}
				}
				if (!is_mom) {
					std::cerr << "  - Dam " << pedigree[i][col_dam] << " is founder " << std::endl;
					pedigree[i][col_dam] = "0";
				}
			}
			if (obs_dad != "0") {
				bool is_dad (false);
				for (std::size_t j(1); j<pedigree.size(); ++j) {
					if (pedigree[j][col_i]==obs_dad) {
						is_dad = true;
						break;
					}
				}
				if (!is_dad) {
					std::cerr << "  - Sire " << pedigree[i][col_sir] << " is founder " << std::endl;
					pedigree[i][col_sir] = "0";
				}
			}
		}		
		
		// Rename by individual ID
		sMatrix tmp = pedigree;
		for (std::size_t i(1); i<pedigree.size(); ++i) {
			name exp = std::to_string(i);
			name obs = pedigree[i][col_i];
			if (obs != exp) {
				for (std::size_t j(1); j<tmp.size(); ++j) {
					if (pedigree[j][col_i]==obs) tmp[j][col_i] = exp;
					if (pedigree[j][col_dam]==obs) tmp[j][col_dam] = exp;
					if (pedigree[j][col_sir]==obs) tmp[j][col_sir] = exp; 
				}
			}
		}

		pedigree = tmp;
	}

	for (std::size_t i(0); i<pedigree.size(); ++i) {
		for (std::size_t j(0); j<pedigree[i].size(); ++j) {
			std::cout << pedigree[i][j];
			if (j<pedigree[i].size()-1) std::cout << ',';
		}	std::cout << std::endl;
	}

	return 0;
}
