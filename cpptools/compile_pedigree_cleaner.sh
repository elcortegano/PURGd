#!/bin/sh

# Compilation instruction for pedigree_cleaner
g++ -std=c++17 -o pedigree_cleaner pedigree_cleaner.cpp ../src/input.cpp ../src/utils.cpp
