#!/bin/sh

# Compilation instruction for pedigree_ancestors
g++ -std=c++17 -o pedigree_ancestors pedigree_ancestors.cpp ../src/input.cpp ../src/utils.cpp
