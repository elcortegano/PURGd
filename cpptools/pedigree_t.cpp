#include "../src/alias.h"
#include "../src/error.h"
#include "../src/templates.h"

sMatrix input_Smatrix (std::string, bool);
coefficient t_i (const sMatrix&, const std::size_t&);
void t_from_parent (const sMatrix&, const std::size_t&, const std::size_t& , iVector&);

constexpr std::size_t col_dam = 1;
constexpr std::size_t col_sir = 2;

/* =======================================================================================
 * pedigree_t: 
 * estimate the equivalent complete generations for a given individual in a pedigree file
 ======================================================================================= */

int main (int argc, char *argv[]) {

	if (argc != 3 && argc != 2) {
		std::cerr << "ERROR: Invalid number of arguments. Check the README file." << std::endl;
		exit(-1);
	}

	name filename = argv[1];
	std::size_t ind (0);
	if (argc==3) ind = atoi(argv[2]);
	sMatrix pedigree = input_Smatrix(filename, true);
	if ((!ind && argc==3) || ind > pedigree.size()-1) {
		std::cerr << "ERROR: Invalid individual index, it must be a number from 1 to N ("<< pedigree.size()-1 << ")." << std::endl;
		exit(-1);
	}

	for (std::size_t i(1); i<pedigree.size(); ++i) {
		if (pedigree[i][0] != std::to_string(i)) {
			std::cerr << "ERROR: Individuals must be named and sorted from 1 to N ("<< pedigree.size()-1 << ")." << std::endl;
			std::cerr << "Run: pedigree_rename " << filename << std::endl;
			exit(-1);
		}
	}

	if (ind) {
		std::cout << "Computing equivalent complete generations for individual " << ind << " (" << filename << ") ...";
		coefficient t = t_i (pedigree, ind);
		std::cout << "\rComputing equivalent complete generations for individual " << ind << " (" << filename << ") ... done!";
		std::cout << std::endl;
		std::cout << "t = " << t << std::endl; 
	} else {
		for (std::size_t i(0); i<pedigree[0].size(); ++i) std::cout << pedigree[0][i] << ',';
		std::cout << 't' << std::endl;
		for (std::size_t ind(1); ind<pedigree.size(); ++ind) {
			coefficient t = t_i (pedigree, ind);
			for (std::size_t j(0); j<pedigree[ind].size(); ++j) {
				std::cout << pedigree[ind][j] << ',';
			}
			std::cout << t << std::endl;
		}
	}

	return 0;
}

coefficient t_i (const sMatrix& pedigree, const std::size_t& index) {
	iVector ancestors;
	t_from_parent(pedigree, index, 0, ancestors);

	coefficient t (0);
	for (const auto& a: ancestors) {
		if (!a) continue;
		t += pow(0.5, a);
	}
	return (t);
}

void t_from_parent (const sMatrix& pedigree, const std::size_t& index, const std::size_t& t, iVector& eval) {
	std::size_t mom = atoi(pedigree[index][col_dam].c_str());
	std::size_t dad = atoi(pedigree[index][col_sir].c_str());

	if (mom) {
		eval.push_back(t+1);
		t_from_parent(pedigree, mom, t+1, eval);
	}
	if (dad) {
		eval.push_back(t+1);
		t_from_parent(pedigree, dad, t+1, eval);
	}
}
